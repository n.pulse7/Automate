import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.Keys;

public class ChromeExample {
    public static void main(String[] args) {

//Setting system properties of ChromeDriver
        System.setProperty("webdriver.chrome.driver", "D://Project//chromedriver_win32//chromedriver.exe");

//Creating an object of ChromeDriver
        ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

//Deleting all the cookies
        driver.manage().deleteAllCookies();

//Specifiying pageLoadTimeout and Implicit wait
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

//launching the specified URL
        driver.get("https://www.Youtube.com/");

//Locating the elements using name locator for the text box
        //driver.findElement(By.name("q")).sendKeys("Youtube");
        //driver.findElement(By.name("pass")).sendKeys("ilovemyfacebookfriends");

//name locator for google search button
        //WebElement searchIcon = driver.findElement(By.name("btnK"));
        //searchIcon.click();

        //WebElement searchIcon1 = driver.findElement(By.className("YouTube"));
        //searchIcon1.click();

        driver.findElement(By.name("search_query")).sendKeys("valorant theme song");
        driver.findElement(By.id("search-icon-legacy")).click();
        driver.findElement(By.id("title-wrapper")).click();











        Actions action = new Actions(driver);
        action.contextClick(driver.findElement(By.xpath("/html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch/div[2]/div[1]/div[1]/ytd-player/div/div[1]/video"))).perform();
       driver.findElement(By.xpath("/html/body/div[6]/div/div/div[1]/div[1]")).click();

    }
}